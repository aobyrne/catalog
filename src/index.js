import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/app'
import { BrowserRouter } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import 'sanitize.css'
import './index.css'

const httpLink = new HttpLink({
  uri: 'https://api.graph.cool/simple/v1/cja4ojkmd1h3n0169by70rotg',
})

const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
})

ReactDOM.render(
  <ApolloProvider client={client}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </ApolloProvider>,
  document.getElementById('root')
)
