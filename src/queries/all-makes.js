import gql from 'graphql-tag'

export default gql`
  query AllMakes {
    allMakes {
      id
      name
    }
  }
`
