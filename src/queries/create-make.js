import gql from 'graphql-tag'

export default gql`
  mutation createMake($name: String!) {
    createMake(name: $name) {
      id
      name
    }
  }
`
