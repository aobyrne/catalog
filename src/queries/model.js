import gql from 'graphql-tag'

export default gql`
  query Model($modelId: ID!) {
    Model(id: $modelId) {
      name
      price
      imageUrl
    }
  }
`
