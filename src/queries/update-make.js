import gql from 'graphql-tag'

export default gql`
  mutation updateMake($id: ID!, $name: String!) {
    updateMake(id: $id, name: $name) {
      id
      name
    }
  }
`
