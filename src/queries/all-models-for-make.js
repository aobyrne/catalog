import gql from 'graphql-tag'

export default gql`
  query ModelsForMake($makeId: String!) {
    allModels(filter: { makeId: $makeId }) {
      id
      name
    }
  }
`
