import gql from 'graphql-tag'

export default gql`
  mutation deleteMake($id: ID!) {
    deleteMake(id: $id) {
      id
      name
    }
  }
`
