import React from 'react'
import Button from './button'
import Input from './input'
import UpdateMakeQuery from '../queries/update-make'
import CreateMakeQuery from '../queries/create-make'
import DeleteMakeQuery from '../queries/delete-make'
import AllMakesQuery from '../queries/all-makes'
import { Flex, Box } from 'grid-styled'
import { graphql, compose } from 'react-apollo'
import { withStateHandlers } from 'recompose'
import styled from 'styled-components'
import update from 'immutability-helper'

const Make = styled.div`
  opacity: ${props => (props.loading ? 0.5 : 1)};
`

const MakeAdmin = withStateHandlers(
  ({ initialNewMakeName = '' }) => ({
    newMakeName: initialNewMakeName,
  }),
  {
    onChangeNewMakeName: ({ newMakeName }) => e => ({
      newMakeName: e.target.value,
    }),
  }
)(
  ({
    newMakeName,
    onChangeNewMakeName,
    createMake,
    updateMake,
    deleteMake,
    data: { allMakes, loading, error },
  }) => {
    if (loading) return <div>Loading...</div>
    if (error) return <div>Error</div>
    return (
      <Flex direction="column">
        <Flex mb={3}>
          <Box w={1}>
            <Input
              type="text"
              value={newMakeName}
              placeholder="Make name..."
              onChange={onChangeNewMakeName}
            />
          </Box>
          <Box>
            <Button
              onClick={e => {
                createMake({
                  variables: { name: newMakeName },
                  optimisticResponse: {
                    createMake: {
                      __typename: 'Make',
                      id: Math.round(Math.random() * -1000000),
                      name: newMakeName,
                    },
                  },
                  update: (store, { data: { createMake } }) => {
                    const data = store.readQuery({ query: AllMakesQuery })
                    store.writeQuery({
                      query: AllMakesQuery,
                      data: update(data, {
                        allMakes: {
                          $push: [createMake],
                        },
                      }),
                    })
                  },
                })
              }}
            >
              Create
            </Button>
          </Box>
        </Flex>
        {allMakes.map((make, index) => (
          <Make loading={make.id < 0} key={make.id}>
            <Flex mb={1}>
              <Box w={1}>
                <Input
                  type="text"
                  placeholder={make.name}
                  onBlur={e => {
                    if (e.target.value && e.target.value !== make.name) {
                      updateMake({
                        variables: {
                          id: make.id,
                          name: e.target.value || make.name,
                        },
                      })
                    }
                  }}
                />
              </Box>
              <Box>
                <Button
                  onClick={() => {
                    deleteMake({
                      variables: { id: make.id },
                      optimisticResponse: {
                        deleteMake: {
                          __typename: 'Make',
                          id: make.id,
                          name: make.name,
                        },
                      },
                      update: (store, { data: { deleteMake } }) => {
                        const data = store.readQuery({ query: AllMakesQuery })
                        store.writeQuery({
                          query: AllMakesQuery,
                          data: update(data, {
                            allMakes: {
                              $splice: [[index, 1]],
                            },
                          }),
                        })
                      },
                    })
                  }}
                >
                  x
                </Button>
              </Box>
            </Flex>
          </Make>
        ))}
      </Flex>
    )
  }
)

const MakeAdminWithQueries = compose(
  graphql(UpdateMakeQuery, { name: 'updateMake' }),
  graphql(CreateMakeQuery, { name: 'createMake' }),
  graphql(DeleteMakeQuery, { name: 'deleteMake' }),
  graphql(AllMakesQuery)
)(MakeAdmin)

export default MakeAdminWithQueries
