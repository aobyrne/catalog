import React from 'react'
import ModelDetails from './model-details'
import Header from './header'

const PageDetail = props => (
  <div>
    <Header>Detail</Header>
    <ModelDetails modelId={props.match.params.id} />
  </div>
)

export default PageDetail
