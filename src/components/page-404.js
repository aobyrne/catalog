import React from 'react'
import Header from './header'

const PageNotFound = () => (
  <div>
    <Header>Page not found</Header>
  </div>
)

export default PageNotFound
