import React from 'react'
import PropTypes from 'prop-types'
import Select from './select'
import { graphql } from 'react-apollo'
import AllMakesQuery from '../queries/all-makes'
import { autobind } from 'core-decorators'

class MakeSelect extends React.Component {
  @autobind
  onChange(event) {
    const id = event.target.value
    const { data: { allMakes } } = this.props
    const make = allMakes.find(make => make.id === id)
    this.props.onChange(make)
  }

  render() {
    const { value, data: { allMakes, loading, error } } = this.props
    return (
      <Select
        value={value || ''}
        options={allMakes}
        loading={loading}
        error={error}
        onChange={this.onChange}
      />
    )
  }
}

MakeSelect.PropTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
}

export default graphql(AllMakesQuery)(MakeSelect)
