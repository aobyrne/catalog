import React from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import { Flex, Box } from 'grid-styled'

const Container = styled(Flex)`
  background-color: #eee;
  height: 4rem;
  align-items: center;
`

const activeClassName = 'active'

const NavItem = styled(NavLink).attrs({
  activeClassName,
})`
  color: #111;
  opacity: 0.5;
  text-decoration: none;
  transition: opacity 0.4s;

  &.${activeClassName} {
    opacity: 1;
    pointer-events: none;
    border-bottom: 2px solid #111;
    font-weight: bold;
  }

  &:hover {
    opacity: 1;
  }
`

const Item = ({ label, path }) => (
  <Box ml={2}>
    <NavItem exact to={path} activeClassName={activeClassName}>
      {label}
    </NavItem>
  </Box>
)

const Nav = () => (
  <Container px={2} justify={['center', 'left']}>
    <Flex>
      <Item label="Home" path="/" />
      <Item label="Search" path="/search" />
      <Item label="Admin" path="/admin" />
    </Flex>
  </Container>
)

export default Nav
