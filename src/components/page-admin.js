import React from 'react'
import AdminMake from './admin-make'
import Header from './header'

const PageAdmin = props => (
  <div>
    <Header>Admin</Header>
    <AdminMake />
  </div>
)

export default PageAdmin
