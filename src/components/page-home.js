import React from 'react'
import ModelDetails from './model-details'
import Header from './header'

const PageHome = () => (
  <div>
    <Header>Home</Header>
    <ModelDetails modelId={'cja4os8j95idd0180s7dmym9g'} />
  </div>
)

export default PageHome
