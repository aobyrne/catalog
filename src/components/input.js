import styled from 'styled-components'

const Input = styled.input`
  width: 100%;
  height: 100%;
  border: 1px solid #ccc;
  outline: none;
  padding-left: 0.5rem;
`

export default Input
