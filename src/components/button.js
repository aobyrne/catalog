import styled from 'styled-components'

export default styled.button`
  width: 100%;
  border: none;
  border-radius: 0;
  background: #333;
  color: white;
  height: 2rem;
  padding: 0 1rem;
  opacity: ${p => (p.disabled ? 0.2 : 0.8)};
  cursor: ${p => (p.disabled ? 'not-allowed' : 'pointer')};
  transition: opacity 0.4s;
  &:hover:not([disabled]) {
    opacity: 1;
  }
`
