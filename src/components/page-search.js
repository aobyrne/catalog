import React, { Component } from 'react'
import Header from './header'
import Button from './button'
import SelectMake from './select-make'
import SelectModel from './select-model'
import { Flex, Box } from 'grid-styled'
import { autobind } from 'core-decorators'

class PageSearch extends Component {
  state = {
    make: { id: null, name: null },
    model: { id: null, name: null },
  }

  @autobind
  onClickDetails() {
    const { make, model } = this.state
    this.props.history.push(`/${make.name}/${model.name}/${model.id}`)
  }

  @autobind
  onChangeMake(make) {
    this.setState({
      make,
      model: { id: null, name: null },
    })
  }

  @autobind
  onChangeModel(model) {
    this.setState({
      model,
    })
  }

  render() {
    const { make, model } = this.state
    return (
      <div>
        <Header>Search</Header>
        <Flex direction={['column', 'row']}>
          <Box w={1} mb={[2, 1]} mr={[0, 2]}>
            <SelectMake value={make.id} onChange={this.onChangeMake} />
          </Box>
          {make.id && (
            <Box w={1} mb={[2, 1]} mr={[0, 2]}>
              <SelectModel
                value={model.id}
                makeId={make.id}
                onChange={this.onChangeModel}
              />
            </Box>
          )}
          <Box w={1}>
            <Button disabled={!model.id} onClick={this.onClickDetails}>
              View Details
            </Button>
          </Box>
        </Flex>
      </div>
    )
  }
}

export default PageSearch
