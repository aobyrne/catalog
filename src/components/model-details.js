import React from 'react'
import PropTypes from 'prop-types'
import { Flex, Box } from 'grid-styled'
import { graphql } from 'react-apollo'
import ModelQuery from '../queries/model'
import styled from 'styled-components'

const Image = styled.img`
  display: block;
  margin: 0 auto;
  max-width: 100%;
`

const Label = styled.span`
  color: #aaa;
`

const ModelDetails = ({ data: { Model, loading, error } }) => {
  if (loading) return <div>Loading...</div>
  if (error) return <div>Error fetching details</div>
  const { name, price, imageUrl } = Model
  const formattedPrice = Number(price).toLocaleString('en-AU', {
    style: 'currency',
    currency: 'AUD',
  })
  return (
    <div>
      <Flex justify={['space-between', 'flex-start']} mb={2}>
        <Box mr={2}>
          <Label>Model / </Label>
          {name}
        </Box>
        <Box>
          <Label>Price / </Label>
          {formattedPrice}
        </Box>
      </Flex>
      <div>
        <Image src={imageUrl} />
      </div>
    </div>
  )
}

ModelDetails.PropTypes = {
  modelId: PropTypes.string.isRequired,
}

export default graphql(ModelQuery)(ModelDetails)
