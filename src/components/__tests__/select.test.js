import React from 'react'
import Select from '../select'
import renderer from 'react-test-renderer'

it('renders correctly', () => {
  const tree = renderer.create(<Select />).toJSON()
  expect(tree).toMatchSnapshot()
})
