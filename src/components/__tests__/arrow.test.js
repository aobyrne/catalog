import React from 'react'
import Arrow from '../arrow'
import renderer from 'react-test-renderer'

it('renders correctly', () => {
  const tree = renderer.create(<Arrow />).toJSON()
  expect(tree).toMatchSnapshot()
})
