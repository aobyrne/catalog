import React from 'react'

const Arrow = () => (
  <svg viewBox="0 0 20 20" width={16} fill="transparent" stroke="#979797">
    <path d="M1,6 L10,15 L19,6" />
  </svg>
)

export default Arrow
