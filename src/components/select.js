import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Arrow from './arrow'

const Container = styled.div`
  position: relative;
`

const ArrowContainer = styled.div`
  position: absolute;
  right: 0.5rem;
  top: 0;
  display: flex;
  height: 100%;
  pointer-events: none;
`

const Select = styled.select`
  width: 100%;
  border: 1px solid #ccc;
  appearance: none;
  border-radius: 0;
  outline: none;
  box-shadow: none;
  height: 2rem;
  padding: 0 0.5rem;
`

const SelectBox = ({ value, options, loading, error, onChange }) => {
  return (
    <Select value={value || ''} onChange={onChange}>
      <option value={''} disabled>
        {loading ? 'Loading...' : 'Select...'}
      </option>
      {options &&
        options.map(({ id, name }) => (
          <option key={id} value={id}>
            {name}
          </option>
        ))}
    </Select>
  )
}

SelectBox.PropTypes = {
  value: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }),
  ).isRequired,
  loading: PropTypes.bool,
  error: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
}

export default props => (
  <Container>
    <SelectBox {...props} />
    <ArrowContainer>
      <Arrow />
    </ArrowContainer>
  </Container>
)
