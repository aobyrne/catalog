import React from 'react'
import Nav from './nav'
import PageHome from './page-home'
import PageSearch from './page-search'
import PageDetail from './page-detail'
import PageAdmin from './page-admin'
import PageNotFound from './page-404'
import { Switch, Route } from 'react-router-dom'
import { Flex, Box } from 'grid-styled'

const App = () => (
  <Flex>
    <Box w={[1, '40em', '52em', '64em']} mx="auto" mb={4}>
      <Nav />
      <Box m={[3, 0]}>
        <Switch>
          <Route exact path="/" component={PageHome} />
          <Route exact path="/search" component={PageSearch} />
          <Route exact path="/:make/:model/:id" component={PageDetail} />
          <Route exact path="/admin" component={PageAdmin} />
          <Route component={PageNotFound} />
        </Switch>
      </Box>
    </Box>
  </Flex>
)

export default App
