import React from 'react'
import PropTypes from 'prop-types'
import Select from './select'
import { graphql } from 'react-apollo'
import AllModelsForMakeQuery from '../queries/all-models-for-make'
import { autobind } from 'core-decorators'

class ModelSelect extends React.Component {
  @autobind
  onChange(event) {
    const id = event.target.value
    const { data: { allModels } } = this.props
    const model = allModels.find(model => model.id === id)
    this.props.onChange(model)
  }

  render() {
    const { value, data: { allModels, loading, error } } = this.props
    return (
      <Select
        value={value || ''}
        options={allModels}
        loading={loading}
        error={error}
        onChange={this.onChange}
      />
    )
  }
}

ModelSelect.PropTypes = {
  value: PropTypes.string.isRequired,
  makeId: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
}

export default graphql(AllModelsForMakeQuery)(ModelSelect)
