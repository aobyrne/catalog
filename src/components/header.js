import React from 'react'
import { Box } from 'grid-styled'
import styled from 'styled-components'

const Container = styled(Box)`
  border-bottom: 1px solid #eee;
`

const Heading = styled.h2`
  color: #ccc;
  margin-bottom: 0.4rem;
  font-weight: normal;
`

const Header = props => (
  <Container mb={3}>
    <Heading>{props.children}</Heading>
  </Container>
)

export default Header
