# Catalog

## Run

  1. `yarn install`
  2. `yarn start`

## Tech

  * React
  * GraphQL, Apollo, Graphcool
  * styled-components, grid-styled
  * Jest
  * Prettier
